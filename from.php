<html>
    <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <form method="post" id="inviteForm">
            Name:
            <input type='text' name='name' id="name" />
            Surname:
            <input type='text' name='surname' id="surname" />
            Food Type:
            <select name='food_type'>
                <option value='Veg'>Veg</option>
                <option value='Non Veg'>Non Veg</option>
            </select>
            <input type="submit" value="submit" id="invite" />
        </form>
    </body>
</html>


 
 
<script>
$('#inviteForm').on('submit', function(ev){
    ev.preventDefault();
    
    $('#giphy').show();
    $('#invite').attr('disabled',true);
    var newurl = "invite.php";
    $.ajax({
        type: "POST",
        url: newurl,
        data: $('#inviteForm').serialize(),
        cache: false,
        success: function(html){
            
            $('#name').val('');
            $('#surname').val('');
            $('#invite').attr('disabled',false);
            $('#giphy').hide();
            if(html == 1){
                $('#inviteForm .formlist').prepend('<li><div class="alert alert-success"> Successfully Invite Robo.</div></li>');
            }else{
                $('#inviteForm .formlist').prepend('<li><div class="alert alert-warning"> Something is wrong please try after some time.</div></li>');
            }
            removeTimeoutDiv();
        },
        error: function() {
            
            $('#robo-phone').val('');
            $('#robo-conference').val('');
            
            $('#giphy').hide();
            $('#invite').attr('disabled',false);
            $('#inviteForm .formlist').prepend('<li><div class="alert alert-warning"> Something is wrong please try after some time.</div></li>');
            removeTimeoutDiv();
        }
    });
});
</script>